package com.kshrd.btbspringsecuritydemo.controller.restcontroller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentRestController {



    @GetMapping("/public")
    public String publicMethod(){
        return "PUBLIC METHOD";
    }

    @GetMapping("/foruser")
    public String normalMethod(){
        return " Hi I am a normal method ...";
    }

    @GetMapping("/foradmin")
    public String adminMethod(){
        return " Hi I am the ADMIN MEthod.";
    }

    @GetMapping("/forcustom")
    public String customMethod(){
        return "<<<< CUSTOM CUSTOME >>>.";
    }

}
