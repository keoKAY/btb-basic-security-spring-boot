package com.kshrd.btbspringsecuritydemo.repository;


import com.kshrd.btbspringsecuritydemo.model.AuthUser;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface StudentRepository {



//    your method for database operation will be written here..

    @Select("Select * from students where username =#{username}")
    @Results(

            {
                  @Result(property = "roles",column = "id",many = @Many(select = "findRolesById"))
            }
    )
    AuthUser findUserByUsername(String username);

    @Select("select  role_name from roles inner join student_role sr on roles.id = sr.role_id where user_id =#{id}")
    List<String> findRolesById(int id);


}
