package com.kshrd.btbspringsecuritydemo.configuration;


import com.kshrd.btbspringsecuritydemo.security.UserDetailServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity

public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    UserDetailServiceImp userDetailService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        super.configure(auth);

//        auth.inMemoryAuthentication().withUser("normal user").password("12345").roles("USER");
//        auth.inMemoryAuthentication().withUser("admin user").password(passwordEncoder().encode("12345")).roles("ADMIN");
//        auth.inMemoryAuthentication().withUser("custom user").password("{noop}12345").roles("CUSTOM");

        auth.userDetailsService(userDetailService)
                .passwordEncoder(passwordEncoder());

    }


    @Bean
    PasswordEncoder passwordEncoder(){
     return new BCryptPasswordEncoder();
   //  return NoOpPasswordEncoder.getInstance();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        super.configure(web);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        super.configure(http);
         http
                 .authorizeRequests()
                 .antMatchers("/public").permitAll()
                 .antMatchers("/foruser").hasRole("USER")
                 .antMatchers("/foradmin").hasAuthority("ADMIN")
                 .antMatchers("/forcustom").hasRole("CUSTOM")
                 .anyRequest()
                 .fullyAuthenticated()
                 .and()
                 .formLogin()
                 .loginProcessingUrl("/login")
                 .and()
                 .logout()
                 .logoutSuccessUrl("/") ;


    }
}
