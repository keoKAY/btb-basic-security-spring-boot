package com.kshrd.btbspringsecuritydemo.security;

import com.kshrd.btbspringsecuritydemo.model.AuthUser;
import com.kshrd.btbspringsecuritydemo.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class UserDetailServiceImp implements UserDetailsService {

//   Work repository

    @Autowired
    StudentRepository studentRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        AuthUser loginUser = studentRepository.findUserByUsername(username);
        if (loginUser==null)
            throw new UsernameNotFoundException("Sorry! This doesn't exist...");


        Collection<? extends GrantedAuthority> authorities =   loginUser.getRoles().stream()
                .map(e-> new SimpleGrantedAuthority(e))
                .collect(Collectors.toList());

        System.out.println("Here is the value of the authorities: ");
        authorities.stream().forEach(System.out::println);

        return new UserDetailsImp(loginUser.getId(),loginUser.getUsername(),loginUser.getPassword(),authorities);
    }
}
